# TASK MANAGER

## DEVELOPER INFO

* **Name**: Maxim Kirillov

* **E-mail**: mkirillov@tsconsulting.com

* **E-mail**: mkirillov@t1-consulting.com

## SOFTWARE

* JDK 8

* IntelliJ IDEA

* MS Windows 10 x64

## HARDWARE

* **RAM**: 16Gb

* **CPU**: i7

* **HDD**: 60Gb

## RUN PROGRAM

```shell
java -jar ./task-manager.jar
```
